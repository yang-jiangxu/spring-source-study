package top.jacktgq.proxy.jdk.mock.v3;

/**
 * @Author CandyWall
 * @Date 2022/4/14--0:04
 * @Description
 */
public interface InvocationHandler {
    void invoke();
}
